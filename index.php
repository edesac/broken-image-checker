<!DOCTYPE html>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript">
        $ = jQuery;
        $(document).ready(function() {
            var $total = 0;

            $('form[name="check-image-urls"]').submit(function(){
                $(this).find('.submit').attr('disabled', 'disabled');
                $('#list-passed').html('');
                $('#list-failed').html('');

                var $lines = $('textarea[name="image-urls"]').val().split('\n');
                $total =  $lines.length;
                $('.processing').html('Checking ' + $total + ' URLs.' );

                for(var $i = 0; $i < $lines.length; $i++){
                    $image = $lines[$i];

                    $id = ($image.split('/').reverse()[0]).replace('.', '_');
                    $('#list').append('<li id="_' + $id + '"></li>');

                    $.ajax({ //Process the form using $.ajax()
                        type      : 'POST', //Method type
                        url       : '/process.php', //Your form processing file URL
                        data      : {'image' : $image, 'idx' : $i}, 
                        success   : function(data) {
                                        arr = data.split('|');
                                        _image = arr[0];
                                        $id = (_image.split('/').reverse()[0]).replace('.', '_');

                                        if (arr[1] != 'OK') {
                                            //$('#list #_'+$id).html(arr[0]);

                                            $('#list-failed').append('<li>' + _image + '</li>')
                                        }
                                        else {
                                            //$('#list #_'+$id).html(arr[1] + ' ' + $id.replace('_', '.'));   
                                            $('#list-passed').append('<li>' + _image + '</li>')
                                        }

                                        $total --;
                                        $('.processing').html('Checking ' + $total + ' URLs.' );
                                        if ($total == 0) {
                                            $('.submit').removeAttr('disabled');
                                        }

                                    },
                        error: function(XMLHttpRequest, textStatus, errorThrown) { 
                            alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                        }                             

                    });      


                }                
                return false;
            });

        });
    </script>
    <style type="text/css">
        table td {
            width: 33%;
            vertical-align: top;
            border: solid #ccc 1px;
            padding-bottom: 20px;
        }

        form input[type="submit"] {
            width: 200px;
            padding: 5px 10px;
            border-radius: 4px;
            margin-top: 5px;
        }
        .processing {
            margin-left: 10px;
        }
    </style>
</head>
<body>
    <table>
        <tr>
            <td>
                <form name="check-image-urls" method="post">
                    <h4>Image URLS (1 per line)<span class="processing"></span></h4>
                    <div><textarea name="image-urls" style="width: 400px; height: 500px;"></textarea></div>
                    <center><input type="submit" class="submit" value="CHECK"></center>
                </form>                
            </td>
            <td>
                <h4>PASSED</h4>
                <ul id="list-passed"></ul>
            </td>
            <td>
                <h4>FAILED</h4>
                <ul id="list-failed"></ul>
            </td>
        </tr>
    </table>
</body>
</html>